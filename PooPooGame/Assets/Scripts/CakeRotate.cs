﻿using UnityEngine;
using System.Collections;

public class CakeRotate : MonoBehaviour {

	Rigidbody playerRigidbody;   

	public float Angle;

	// Use this for initialization
	void Start () {

		playerRigidbody = GetComponent <Rigidbody> ();
	}

	// Update is called once per frame
	void Update () {
		playerRigidbody.transform.Rotate(0, Angle, 0);
	}

	void Move (float h)
	{
	}

}