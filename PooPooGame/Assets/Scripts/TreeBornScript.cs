﻿using UnityEngine;

public class TreeBornScript : MonoBehaviour
{
   // public PlayerHealth playerHealth;       // Reference to the player's heatlh.
    public GameObject tree;                // The enemy prefab to be spawned.
    public float spawnTime;            // How long between each spawn.
    public Transform[] spawnPoints;         // An array of the spawn points this enemy can spawn from.


    void Start ()
    {
        InvokeRepeating ("Spawn", spawnTime, spawnTime);
    }
    void Spawn ()
    {
        int spawnPointIndex = Random.Range (0, spawnPoints.Length);
        Instantiate (tree, spawnPoints[spawnPointIndex].position, spawnPoints[spawnPointIndex].rotation);
    }
}
