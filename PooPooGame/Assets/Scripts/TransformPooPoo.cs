﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TransformPooPoo : MonoBehaviour {
	//public AudioSource UpgradeMusic;
	[System.Serializable]

	public class RocksEntry
	{
		public GameObject breakNode;
		public float transformScore;
	}

	public List<RocksEntry> RockSettings;

	void Update(){

		foreach (RocksEntry entry in RockSettings) {
			if (ScoreManagementScript.score > entry.transformScore) {
				entry.breakNode.gameObject.SetActive (true);
				//UpgradeMusic.Play ();
			}
			if (ScoreManagementScript.score < entry.transformScore) {
				entry.breakNode.gameObject.SetActive (false);
			}
		}

	}
		
}