﻿using UnityEngine;
using System.Collections;

public class donutsManagement : MonoBehaviour {

	Rigidbody playerRigidbody;  
	Vector3 movement; 
	public float speed = 50f;   

	// Use this for initialization
	void Start () {

		playerRigidbody = GetComponent <Rigidbody> ();
	}
	
	// Update is called once per frame
	void Update () {

		if ( TimerManagementScript.timeLeft <= 3.63) {
			Move (-1);
		}

		if (playerRigidbody.position.z >= 30f) {
			Application.LoadLevel("End2");
		}
	}

	void Move (float h)
	{
		movement.Set (h, 0f, h);
		movement = movement.normalized * Time.deltaTime * speed;	  
		playerRigidbody.MovePosition (transform.position - movement);
	}
}
	