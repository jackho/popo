﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Collections;

public class HighScore : MonoBehaviour {

	public int highScore = 0;
	//public GameUIManager uiManager;

	Text text;   

	void Awake ()
	{
		// Set up the reference.
		text = GetComponent <Text> ();
	}

	public void OnEnable()
	{
		Debug.Log ("Enabled");
		if (ES2.Exists ("High Score")) {
			this.highScore = ES2.Load<int> ("High Score");
			//uiManager.SetHp (this.highScore);
		}
	}

	public void OnDisable()
	{
		Debug.Log ("Disabled");
		ES2.Save<int> (this.highScore, "High Score");
	}

	void Update ()
	{
		// Set the displayed text to be the word "Score" followed by the score value.

		if(ScoreManagementScript.score > highScore){highScore = ScoreManagementScript.score;}

		text.text = "High Score: " + highScore;
	}
}
