﻿using UnityEngine;
using System.Collections;

public class CakeDrop : MonoBehaviour {

	Rigidbody playerRigidbody;     
	Vector3 movement; 
	public float speed = 5f;   

	// Use this for initialization
	void Start () {

		playerRigidbody = GetComponent <Rigidbody> ();
	}

	// Update is called once per frame
	void Update () {

		if (playerRigidbody.position.y <= -29f) {
			Application.LoadLevel("End3");
		}

		//PlayerAudio.Play ();
		float h = 1;

		if (playerRigidbody.position.x >= 3.6f) {
			Move (h);
		}
	}
		
	void Move (float h)
	{
		movement.Set (h, 0f, 0f);
		movement = movement.normalized * speed * Time.deltaTime;	  
		playerRigidbody.MovePosition (transform.position - movement);

	}
}
