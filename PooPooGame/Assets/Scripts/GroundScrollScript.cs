﻿using UnityEngine;
using System.Collections;

public class GroundScrollScript : MonoBehaviour {
	public float speed;
	void Update(){
		//Debug.Log ("hello");
		GetComponent<Renderer>().material.mainTextureOffset = new Vector2(0, Time.time * speed);
	}
}