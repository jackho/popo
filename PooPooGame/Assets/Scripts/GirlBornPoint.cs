﻿using UnityEngine;

public class GirlBornPoint : MonoBehaviour
{
	// public PlayerHealth playerHealth;       // Reference to the player's heatlh.
	public GameObject Girl;                // The enemy prefab to be spawned.
	public float spawnTime;            // How long between each spawn.
	public Transform[] spawnPoints;         // An array of the spawn points this enemy can spawn from.
	void Start ()
	{
		InvokeRepeating ("Spawn", spawnTime, spawnTime);
	}

	void Spawn ()
	{
		if (TimerManagementScript.timeLeft > 7) {
			int spawnPointIndex = Random.Range (0, spawnPoints.Length);
			Instantiate (Girl, spawnPoints [spawnPointIndex].position, spawnPoints [spawnPointIndex].rotation);
		}
	}
}