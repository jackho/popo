﻿using UnityEngine;
using System.Collections;

public class CakeManagement : MonoBehaviour {
    Rigidbody playerRigidbody;  
	Vector3 movement; 
	public float speed = 30f;   
	public AudioSource MotzMusic;
	public AudioSource GirlMusic;
	public AudioSource TreeMusic;
	public AudioSource TrollMusic;
	public AudioSource UpgradeMusic;
	private const float lowPassFilterFactor = 0.2f;
	int count=1;

	void Start () 
	{
        playerRigidbody = GetComponent <Rigidbody> ();
		//设置设备陀螺仪的开启/关闭状态，使用陀螺仪功能必须设置为 true  
		Input.gyro.enabled = true;  
		//获取设备重力加速度向量  
		Vector3 deviceGravity = Input.gyro.gravity;  
		//设备的旋转速度，返回结果为x，y，z轴的旋转速度，单位为（弧度/秒）  
		Vector3 rotationVelocity = Input.gyro.rotationRate;  
		//获取更加精确的旋转  
		Vector3 rotationVelocity2 = Input.gyro.rotationRateUnbiased;  
		//设置陀螺仪的更新检索时间，即隔 0.1秒更新一次  
		Input.gyro.updateInterval = 0.01f;  
		//获取移除重力加速度后设备的加速度  
		Vector3 acceleration = Input.gyro.gravity; 

	}
	void Update () 
	{

		float h = Input.GetAxisRaw ("Horizontal");
		Vector3 way = Input.gyro.gravity; 

		//print (way.x);

//		if (way.x > 0) {
//			h = 1;
//		} else if (way.x < 0) {
//			h = -1;
//		}
//		else {
//			h = 0;
//		}
		    
		//h = way.x * 3;

		if(ScoreManagementScript.score>400&&count==1){
			UpgradeMusic.Play ();
			count += 1;
		}
		else if(ScoreManagementScript.score>800&&count==2){
			UpgradeMusic.Play ();
			count += 1;
		}
		else if(ScoreManagementScript.score>1200&&count==3){
			UpgradeMusic.Play ();
			count += 1;
		}
		else if(ScoreManagementScript.score>1600&&count==4){
			UpgradeMusic.Play ();
			count += 1;
		}
		else if(ScoreManagementScript.score>2000&&count==5){
			UpgradeMusic.Play ();
			count += 1;
		}
		else if(ScoreManagementScript.score>2400&&count==6){
			UpgradeMusic.Play ();
			count += 1;
		}

		if(ScoreManagementScript.score<400&&count==2){
			count -= 1;
		}
		else if(ScoreManagementScript.score<800&&count==3){
			count -= 1;
		}
		else if(ScoreManagementScript.score<1200&&count==4){
			count -= 1;
		}
		else if(ScoreManagementScript.score<1600&&count==5){
			count -= 1;
		}
		else if(ScoreManagementScript.score<2000&&count==6){
			count -= 1;
		}
		else if(ScoreManagementScript.score<2400&&count==7){
			count -= 1;
		}

		if (playerRigidbody.position.x <= -27f) {
			if (h > 0) {h = 0;ScoreManagementScript.score -= 5;
				//MotzMusic.Play ();
			}
		}
		else if (playerRigidbody.position.x >= 34f ) {
			if (h < 0){h = 0;ScoreManagementScript.score -= 5;  
				//MotzMusic.Play ();			
			}
		}

		Move (h);
	}

	void Move (float h)
	{
		movement.Set (h, 0f, 0f);
		movement = movement.normalized * speed * Time.deltaTime;	  
		playerRigidbody.MovePosition (transform.position - movement);
	    
	}
	void OnTriggerEnter(Collider other) 
	{
		if (other.name == "Izzy(Clone)" ) 
		{
			TimerManagementScript.timeLeft += 2;
			Destroy(other.gameObject);
			GirlMusic.Play ();
		}
		if (other.name == "Troll(Clone)" ) 
		{
			ScoreManagementScript.score -= 100;
			Destroy(other.gameObject);
			TrollMusic.Play ();
		}
        if (other.name == "Bush 01 prefab(Clone)")  
		{
			ScoreManagementScript.score += 30;
			Destroy(other.gameObject);
			TreeMusic.Play ();
		}
		if (other.name == "Bush 02 prefab(Clone)")  
		{
			ScoreManagementScript.score += 100;
			Destroy(other.gameObject);
			TreeMusic.Play ();
		}
		if (other.name == "Bush 03 prefab(Clone)")
		{
			ScoreManagementScript.score += 50;
			Destroy(other.gameObject);
			TreeMusic.Play ();
		}
	}
}