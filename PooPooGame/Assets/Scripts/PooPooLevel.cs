﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PooPooLevel : MonoBehaviour {

	public static int score = ScoreManagementScript.score;        // The player's score.

	Text text;                // Reference to the Text component.

	void Awake ()
	{
		// Set up the reference.
		text = GetComponent <Text> ();
	}
	void Update ()
	{
		// Set the displayed text to be the word "Score" followed by the score value.
		if (score >= 2400) {
			text.text = "PooPoo Master";
		} else if (score >= 2000) {
			text.text = "PooPoo MVP";
		} else if (score >= 1600) {
			text.text = "PooPoo Professor";
		} else if (score >= 1200) {
			text.text = "PooPoo Allstar";
		} else if (score >= 800) {
			text.text = "PooPoo Expert";
		} else if (score >= 400) {
			text.text = "PooPoo Teacher";
		} else {
			text.text = "PooPoo Rookie";
		}
	}
}
