﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
 
 public class TimerManagementScript : MonoBehaviour
 {
	 public static float timeLeft = 30.00f; 
     Text text; 

     void Awake ()
     {
		text = GetComponent <Text> ();
		timeLeft = 30;
     }     
     void Update()
     {
         timeLeft -= Time.deltaTime;
        
		if (timeLeft <= 0) {
			//print ("End");
			//Application.LoadLevel("End");

			text.text = "00.00";

		} else {
			text.text = timeLeft.ToString ("00.00");
		}
     }
 }